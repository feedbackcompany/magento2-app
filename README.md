# The Feedback Company Connect for Magento® 2

The Magento Feedback Company connector is a Magento® 2 extension that fully integrates your Feedback Company data into your Magento® 2 store, this allows quick and simple management of reviews (inc. automated synchronization of older reviews and client details), manage your current reviews and fully automate review invitations all from your Magento® 2 Back-end.
